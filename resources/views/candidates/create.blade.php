@extends('layouts.app')

@section('title', 'Create candidate')

@section('content')  

        <h1>Create candidate</h1>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div class="form-group mx-sm-3 mb-2">
            <h5><label for = "name">Candiadte name</label></h5>
            <input type = "text" class="form-control" name = "name">
        </div>     
        <div class="form-group mx-sm-3 mb-2">
            <h5><label for = "email">Candiadte email</label></h5>
            <input type = "text" class="form-control" name = "email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div> 
        <div>
            <button type="submit" class="btn btn-primary mb-2">Create candidate</button>
        </div>                       
        </form>    

@endsection