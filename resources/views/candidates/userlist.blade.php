@extends('layouts.app')

@section('title', 'Users')

@section('content')

    <h1>Users List</h1>
    <table class = "table table-dark">
        <tr>
            <th>Id</th><th>Name</th><th>Department</th><th></th><th></th><th>Role</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->department->name}}</td>
            <td><a href = "{{route('user.delete',$user->id)}}">Delete</a></td>
            <td><a href = "{{route('candidates.userdetails',$user->id)}}">Details</a></td>
            <td>
                @if(isset($roles->id))
                    {{$user->roles->name}}  
                @else
                No Role
                @endif
            </td>
        </tr>
        @endforeach
    </table>
    @endsection
           
            
        
