@extends('layouts.app')

@section('title', 'Candidate')

@section('content')

    <h1>User details</h1>
    <table class = "table table-dark">
        <tr>
            <td>Id</td>
            <td>{{$user->id}}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{$user->email}}</td>
        </tr> 
        <tr>
        <tr>
            <td>Department</td>
            <td>
                <form method="POST" action="{{ route('candidates.changedepartment') }}">          
                    @csrf
            <div class="col-md-6">
                @if (App\User::isAdmin($user) != false ) 
                    {{$user->department->name}}
                    <select class="form-control" name="department_id">
                        @foreach (App\User::isAdmin($user->department_id) as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach 
                    </select>
                    <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Change department
                            </button>
                        </div>
                    </div>  
                </td>
                @else
                <td>{{$user->department->name}}</td>
                @endif  
        </tr>    
            <td>Created</td>
            <td>{{$user->created_at}}</td>
        </tr>
        <tr>
            <td>Updated</td>
            <td>{{$user->updated_at}}</td>  
        </tr>    
    </table>
    @endsection